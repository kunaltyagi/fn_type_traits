## Making functions first-class type traits

Currently, `boost::function_traits` and c++17 <type_trait> don't treat function types as
first class citizens for traits.

This is an attempt to provide detection and manipulation of function type to aid in
SFINAE, CRTP, and `constexpr if`. (Should also help if the compile-time reflection comes to C++)

## Similarities
### With standard
* Want to check if a function is reference? Just replace `is_reference<T>` with `is_reference_fn<T>`

### With Boost::function_traits
* Doesn't work for function pointers. Requires `add_pointer` and `remove_pointer` for that. Which don't work well with classes in case of Boost and with functions in case of standard

## Differences
### From standard
Everything. Nothing in this library is defined in standard

### From Boost::function_traits
* This library aims to stick close to naming the traits close to type_traits defined in the standard. This means `type_trait<Type>::type` or `type_trait<Type>::value` (depending on requirement) instead of `function_traits<Type>::type_trait`

* Best for the last: This works for member functions as well as free functions.

## Details
### Detection
* `is_const_fn<decltype(some_fn)>`: Detect if `decltype(some_fn) == return(argument_list...) const [volatile, &|&&, noexcept]`
* `is_const_fn_v<T>`: helper template
* `is_volatile_fn<decltype(some_fn)>`: Detect if `decltype(some_fn) == return(argument_list...) volatile [const, &|&&, noexcept]`
* `is_volatile_fn_v<T>`: helper template
* `is_noexcept_fn<decltype(some_fn)>`: Detect if `decltype(some_fn) == return(argument_list...) [const, volatile, &|&&] noexcept`
* `is_noexcept_fn_v<T>`: helper template
* `is_lvalue_reference_fn<decltype(some_fn)>`: Detect if `decltype(some_fn) == return(argument_list...) [const, volatile] & [noexcept]`
* `is_lvalue_reference_fn_v<T>`: helper template
* `is_rvalue_reference_fn<decltype(some_fn)>`: Detect if `decltype(some_fn) == return(argument_list...) [const, volatile] && [noexcept]`
* `is_rvalue_reference_fn_v<T>`: helper template

### Modify
* add_const_fn, remove_const_fn
* add_volatile_fn, remove_volatile_fn
* add_noexcept_fn, remove_noexcept_fn
* add_lvalue_reference_fn
* add_rvalue_reference_fn
* remove_reference_fn
* remove_cv_fn, remove_cvref_fn
* With helper templates of add_*_fn_t, remove_*_fn_t

## Status
### Current
Basic functionlity works and is tested.

Most of the functionality is added, but not tested.

### TODO
* check if class has a function with signature
* tests for reference related functions
* tests for composites like cv, cvref
* add const to return type, some argument
* duplicate the minor capability of `boost::function_traits`: get the arg, ret and arity
* What's the deal with functors and std::fn?
* What about mutable?
* remove and add pointer
* Just check if a function happends, drop the arguments

## License
Wherever not mentioned, MIT license.

## Contribute
Fork, edit, send a pull-request
