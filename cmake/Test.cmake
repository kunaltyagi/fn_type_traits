find_package(Threads REQUIRED)

set(GTEST_FOUND FALSE)
set(GTEST-NOTFOUND TRUE)

if(NOT DEFINED GTEST_ROOT)
  set(GTEST_ROOT "${PROJECT_SOURCE_DIR}/third_party/googletest" CACHE PATH "Path to googletest")
  message(WARNING "No GTEST_ROOT variable defined in parent CMake file. Using default value: " ${GTEST_ROOT})
endif(NOT DEFINED GTEST_ROOT)
set(googlemock_source_dir "${GTEST_ROOT}/googlemock")
set(googletest_source_dir "${GTEST_ROOT}/googletest")

add_library(libgtest STATIC
    "${googlemock_source_dir}/src/gmock-all.cc"
    "${googletest_source_dir}/src/gtest-all.cc"
)
target_link_libraries(libgtest ${CMAKE_THREAD_LIBS_INIT})
target_include_directories(libgtest SYSTEM
    PRIVATE "${googlemock_source_dir}"
    PRIVATE "${googletest_source_dir}"
)

add_library(libgtest-main STATIC
    "${googlemock_source_dir}/src/gmock_main.cc"
)
target_link_libraries(libgtest-main libgtest)
target_include_directories(libgtest-main SYSTEM
    PRIVATE "${googlemock_source_dir}"
    PRIVATE "${googletest_source_dir}"
)

set(GTEST_FOUND TRUE)
set(GTEST_INCLUDE_DIRS
    "${googlemock_source_dir}/include"
    "${googletest_source_dir}/include"
)
set(GTEST_BOTH_LIBRARIES libgtest libgtest-main)
set(GTEST_LIBRARIES libgtest)
set(GTEST_MAIN_LIBRARIES libgtest-main)
