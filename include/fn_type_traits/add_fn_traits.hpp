#ifndef _ADD_FN_TRAITS_HPP_
#define _ADD_FN_TRAITS_HPP_

#include <type_traits>

#include "base_fn_traits.hpp"

namespace fn_type_traits {
namespace detail {
template <class>
struct add_const_function;
template <class>
struct add_volatile_function;
template <class>
struct add_noexcept_function;
template <class>
struct add_lvalue_ref_function;
template <class>
struct add_rvalue_ref_function;
}  // namespace detail

template <class T>
struct add_const_fn : detail::apply_trait_fn<detail::add_const_function, T> {};
template <class T>
using add_const_fn_t = typename add_const_fn<T>::type;

template <class T>
struct add_volatile_fn
    : detail::apply_trait_fn<detail::add_volatile_function, T> {};
template <class T>
using add_volatile_fn_t = typename add_volatile_fn<T>::type;

template <class T>
struct add_noexcept_fn
    : detail::apply_trait_fn<detail::add_noexcept_function, T> {};
template <class T>
using add_noexcept_fn_t = typename add_noexcept_fn<T>::type;

template <class T>
struct add_lvalue_reference_fn
    : detail::apply_trait_fn<detail::add_lvalue_ref_function, T> {};
template <class T>
using add_lvalue_reference_fn_t = typename add_lvalue_reference_fn<T>::type;

template <class T>
struct add_rvalue_reference_fn
    : detail::apply_trait_fn<detail::add_rvalue_ref_function, T> {};
template <class T>
using add_rvalue_reference_fn_t = typename add_rvalue_reference_fn<T>::type;

template <class T>
struct add_cv_fn : add_const_fn<add_volatile_fn_t<T>> {};
template <class T>
using add_cv_fn_t = typename add_cv_fn<T>::type;

namespace detail {
template <class T>
struct add_const_function {
    using type = T;
};

template <class T>
struct add_volatile_function {
    using type = T;
};

template <class T>
struct add_noexcept_function {
    using type = T;
};

template <class T>
struct add_lvalue_ref_function {
    using type = T;
};

template <class T>
struct add_rvalue_ref_function {
    using type = T;
};

/**
 * Specialization for const
 */
// normal fn
template <class Ret, class... Args>
struct add_const_function<Ret(Args...)> {
    using type = Ret(Args...) const;
};
// variadic fn
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...)> {
    using type = Ret(Args..., ...) const;
};

// volatile
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile> {
    using type = Ret(Args...) const volatile;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile> {
    using type = Ret(Args..., ...) const volatile;
};
// ref qualified
template <class Ret, class... Args>
struct add_const_function<Ret(Args...)&> {
    using type = Ret(Args...) const&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...)&> {
    using type = Ret(Args..., ...) const&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile&> {
    using type = Ret(Args...) const volatile&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile&> {
    using type = Ret(Args..., ...) const volatile&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) &&> {
    using type = Ret(Args...) const&&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) &&> {
    using type = Ret(Args..., ...) const&&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile&&> {
    using type = Ret(Args...) const volatile&&;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile&&> {
    using type = Ret(Args..., ...) const volatile&&;
};
// noexcept
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) noexcept> {
    using type = Ret(Args...) const noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) noexcept> {
    using type = Ret(Args..., ...) const noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile noexcept> {
    using type = Ret(Args...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile noexcept> {
    using type = Ret(Args..., ...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) & noexcept> {
    using type = Ret(Args...) const& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) & noexcept> {
    using type = Ret(Args..., ...) const& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile& noexcept> {
    using type = Ret(Args...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile& noexcept> {
    using type = Ret(Args..., ...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) && noexcept> {
    using type = Ret(Args...) const&& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) && noexcept> {
    using type = Ret(Args..., ...) const&& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args...) volatile&& noexcept> {
    using type = Ret(Args...) const volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_const_function<Ret(Args..., ...) volatile&& noexcept> {
    using type = Ret(Args..., ...) const volatile&& noexcept;
};

/**
 * Specialization for volatile
 */
// normal fn
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...)> {
    using type = Ret(Args...) volatile;
};
// variadic fn
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...)> {
    using type = Ret(Args..., ...) volatile;
};

// const
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const> {
    using type = Ret(Args...) const volatile;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const> {
    using type = Ret(Args..., ...) const volatile;
};
// ref qualified
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...)&> {
    using type = Ret(Args...) volatile&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...)&> {
    using type = Ret(Args..., ...) volatile&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const&> {
    using type = Ret(Args...) const volatile&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const&> {
    using type = Ret(Args..., ...) const volatile&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) &&> {
    using type = Ret(Args...) volatile&&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) &&> {
    using type = Ret(Args..., ...) volatile&&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const&&> {
    using type = Ret(Args...) const volatile&&;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const&&> {
    using type = Ret(Args..., ...) const volatile&&;
};
// noexcept
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) noexcept> {
    using type = Ret(Args...) volatile noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) noexcept> {
    using type = Ret(Args..., ...) volatile noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const noexcept> {
    using type = Ret(Args...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const noexcept> {
    using type = Ret(Args..., ...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) & noexcept> {
    using type = Ret(Args...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) & noexcept> {
    using type = Ret(Args..., ...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const& noexcept> {
    using type = Ret(Args...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const& noexcept> {
    using type = Ret(Args..., ...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) && noexcept> {
    using type = Ret(Args...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) && noexcept> {
    using type = Ret(Args..., ...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args...) const&& noexcept> {
    using type = Ret(Args...) const volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_volatile_function<Ret(Args..., ...) const&& noexcept> {
    using type = Ret(Args..., ...) const volatile&& noexcept;
};

/**
 * Specialization for noexcept
 */
// normal fn
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...)> {
    using type = Ret(Args...) noexcept;
};
// variadic fn
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...)> {
    using type = Ret(Args..., ...) noexcept;
};

// const
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const> {
    using type = Ret(Args...) const noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const> {
    using type = Ret(Args..., ...) const noexcept;
};
// ref qualified
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...)&> {
    using type = Ret(Args...) & noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...)&> {
    using type = Ret(Args..., ...) & noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const&> {
    using type = Ret(Args...) const& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const&> {
    using type = Ret(Args..., ...) const& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) &&> {
    using type = Ret(Args...) && noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) &&> {
    using type = Ret(Args..., ...) && noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const&&> {
    using type = Ret(Args...) const&& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const&&> {
    using type = Ret(Args..., ...) const&& noexcept;
};
// volatile
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) volatile> {
    using type = Ret(Args...) volatile noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) volatile> {
    using type = Ret(Args..., ...) volatile noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const volatile> {
    using type = Ret(Args...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const volatile> {
    using type = Ret(Args..., ...) const volatile noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) volatile&> {
    using type = Ret(Args...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) volatile&> {
    using type = Ret(Args..., ...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const volatile&> {
    using type = Ret(Args...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const volatile&> {
    using type = Ret(Args..., ...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) volatile&&> {
    using type = Ret(Args...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) volatile&&> {
    using type = Ret(Args..., ...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args...) const volatile&&> {
    using type = Ret(Args...) const volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_noexcept_function<Ret(Args..., ...) const volatile&&> {
    using type = Ret(Args..., ...) const volatile&& noexcept;
};

/**
 * Specialization for lvalue reference
 */
// normal fn
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...)> {
    using type = Ret(Args...) &;
};
// variadic fn
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...)> {
    using type = Ret(Args..., ...) &;
};

// const
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const> {
    using type = Ret(Args...) const&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const> {
    using type = Ret(Args..., ...) const&;
};
// volatile
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) volatile> {
    using type = Ret(Args...) volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) volatile> {
    using type = Ret(Args..., ...) volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const volatile> {
    using type = Ret(Args...) const volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const volatile> {
    using type = Ret(Args..., ...) const volatile&;
};
// ref qualified
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) &&> {
    using type = Ret(Args...) &;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) &&> {
    using type = Ret(Args..., ...) &;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const&&> {
    using type = Ret(Args...) const&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const&&> {
    using type = Ret(Args..., ...) const&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) volatile&&> {
    using type = Ret(Args...) volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) volatile&&> {
    using type = Ret(Args..., ...) volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const volatile&&> {
    using type = Ret(Args...) const volatile&;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const volatile&&> {
    using type = Ret(Args..., ...) const volatile&;
};
// noexcept
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) noexcept> {
    using type = Ret(Args...) & noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) noexcept> {
    using type = Ret(Args..., ...) & noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const noexcept> {
    using type = Ret(Args...) const& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const noexcept> {
    using type = Ret(Args..., ...) const& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) volatile noexcept> {
    using type = Ret(Args...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) volatile noexcept> {
    using type = Ret(Args..., ...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const volatile noexcept> {
    using type = Ret(Args...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const volatile noexcept> {
    using type = Ret(Args..., ...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) && noexcept> {
    using type = Ret(Args...) & noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) && noexcept> {
    using type = Ret(Args..., ...) & noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const&& noexcept> {
    using type = Ret(Args...) const& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const&& noexcept> {
    using type = Ret(Args..., ...) const& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) volatile&& noexcept> {
    using type = Ret(Args...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) volatile&& noexcept> {
    using type = Ret(Args..., ...) volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args...) const volatile&& noexcept> {
    using type = Ret(Args...) const volatile& noexcept;
};
template <class Ret, class... Args>
struct add_lvalue_ref_function<Ret(Args..., ...) const volatile&& noexcept> {
    using type = Ret(Args..., ...) const volatile& noexcept;
};

/**
 * Specialization for rvalue reference
 */
// normal fn
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...)> {
    using type = Ret(Args...) &&;
};
// variadic fn
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...)> {
    using type = Ret(Args..., ...) &&;
};

// const
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) const> {
    using type = Ret(Args...) const&&;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) const> {
    using type = Ret(Args..., ...) const&&;
};
// volatile
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) volatile> {
    using type = Ret(Args...) volatile&&;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) volatile> {
    using type = Ret(Args..., ...) volatile&&;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) const volatile> {
    using type = Ret(Args...) const volatile&&;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) const volatile> {
    using type = Ret(Args..., ...) const volatile&&;
};
// noexcept
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) noexcept> {
    using type = Ret(Args...) && noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) noexcept> {
    using type = Ret(Args..., ...) && noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) const noexcept> {
    using type = Ret(Args...) const&& noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) const noexcept> {
    using type = Ret(Args..., ...) const&& noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) volatile noexcept> {
    using type = Ret(Args...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) volatile noexcept> {
    using type = Ret(Args..., ...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args...) const volatile noexcept> {
    using type = Ret(Args...) const volatile&& noexcept;
};
template <class Ret, class... Args>
struct add_rvalue_ref_function<Ret(Args..., ...) const volatile noexcept> {
    using type = Ret(Args..., ...) const volatile&& noexcept;
};
}  // namespace detail
}  // namespace fn_type_traits
#endif  // ifndef _ADD_FN_TRAITS_HPP_
