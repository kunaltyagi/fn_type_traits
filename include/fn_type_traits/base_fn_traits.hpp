#ifndef _BASE_FN_TRAITS_HPP_
#define _BASE_FN_TRAITS_HPP_

#include <type_traits>

namespace fn_type_traits {
namespace detail {
#define _VALUE_TYPE_ONE_(TraitClass) \
    template <class T>               \
    inline constexpr bool TraitClass##_v = TraitClass<T>::value;
#define _DATA_TYPE_ONE_(TraitClass) \
    template <class T>              \
    inline constexpr bool TraitClass##_t = typename TraitClass<T>::type;
#define _VALUE_TYPE_TWO_(TraitClass) \
    template <class T, class U>      \
    inline constexpr bool TraitClass##_v = TraitClass<T, U>::value;
#define _DATA_TYPE_TWO_(TraitClass) \
    template <class T, class U>     \
    inline constexpr bool TraitClass##_t = typename TraitClass<T, U>::type;

template <class Type>
struct strip_class {
    using type = Type;
};
template <class T>
using strip_class_t = typename strip_class<T>::type;

template <class Class, class Member>
struct strip_class<Member Class::*> {
    using type = Member;
};
}  // namespace detail

template <class T>
struct is_fn : std::is_function<detail::strip_class_t<T>> {};
template <class T>
inline constexpr bool is_fn_v = is_fn<T>::value;

template <class Type>
struct is_non_class : std::is_same<Type, detail::strip_class_t<Type>> {};
template <class T>
inline constexpr bool is_non_class_v = is_non_class<T>::value;

template <class Type>
struct is_free_fn : std::conjunction<is_fn<Type>, is_non_class<Type>> {};
template <class T>
inline constexpr bool is_free_fn_v = is_free_fn<T>::value;

namespace detail {
template <class Type>
using is_free = is_non_class<Type>;
template <class T>
inline constexpr bool is_free_v = is_free<T>::value;

template <class SrcType, class DstType>
struct copy_class {
    using type = DstType;
};
template <class SrcType, class DstType>
using copy_class_t = typename copy_class<SrcType, DstType>::type;

template <class SrcClass, class SrcMember, class DstType>
struct copy_class<SrcMember SrcClass::*, DstType> {
    using type = DstType SrcClass::*;
};

template <class SrcClass, class SrcMember, class DstClass, class DstMember>
struct copy_class<SrcMember SrcClass::*, DstMember DstClass::*> {
    using type = DstMember SrcClass::*;
};

template <class SrcType, class DstClass, class DstMember>
struct copy_class<SrcType, DstMember DstClass::*> {
    using type = DstMember;
};

template <template <class> class Trait, class Type>
struct apply_trait_fn
    : copy_class<Type, typename Trait<strip_class_t<Type>>::type> {};
template <template <class> class Trait, class Type>
using apply_trait_fn_t = typename apply_trait_fn<Trait, Type>::type;
}  // namespace detail
}  // namespace fn_type_traits
#endif  // ifndef _BASE_FN_TRAITS_HPP_
