#ifndef _DETECT_FN_TRAITS_HPP_
#define _DETECT_FN_TRAITS_HPP_

#include <type_traits>

#include "base_fn_traits.hpp"

namespace fn_type_traits {
namespace detail {
template <class>
struct is_const_function;
template <class>
struct is_volatile_function;
template <class>
struct is_noexcept_function;
template <class>
struct is_lvalue_ref_function;
template <class>
struct is_rvalue_ref_function;
}  // namespace detail

template <class T>
struct is_const_fn : detail::is_const_function<detail::strip_class_t<T>> {};
template <class T>
inline constexpr bool is_const_fn_v = is_const_fn<T>::value;

template <class T>
struct is_volatile_fn : detail::is_volatile_function<detail::strip_class_t<T>> {
};
template <class T>
inline constexpr bool is_volatile_fn_v = is_volatile_fn<T>::value;

template <class T>
struct is_noexcept_fn : detail::is_noexcept_function<detail::strip_class_t<T>> {
};
template <class T>
inline constexpr bool is_noexcept_fn_v = is_noexcept_fn<T>::value;

template <class T>
struct is_lvalue_reference_fn : detail::is_lvalue_ref_function<detail::strip_class_t<T>> {
};
template <class T>
inline constexpr bool is_lvalue_reference_fn_v = is_lvalue_reference_fn<T>::value;

template <class T>
struct is_rvalue_reference_fn : detail::is_rvalue_ref_function<detail::strip_class_t<T>> {
};
template <class T>
inline constexpr bool is_rvalue_reference_fn_v = is_rvalue_reference_fn<T>::value;

template <class T>
struct is_cv_fn : std::disjunction<is_const_fn<T>, is_volatile_fn<T>> {};
template <class T>
inline constexpr bool is_cv_fn_v = is_cv_fn<T>::value;

template <class T>
struct is_reference_fn : std::disjunction<is_lvalue_reference_fn<T>, is_rvalue_reference_fn<T>> {};
template <class T>
inline constexpr bool is_reference_fn_v = is_reference_fn<T>::value;

namespace detail {
template <class>
struct is_const_function : std::false_type {};

template <class>
struct is_volatile_function : std::false_type {};

template <class>
struct is_noexcept_function : std::false_type {};

template <class>
struct is_lvalue_ref_function : std::false_type {};

template <class>
struct is_rvalue_ref_function : std::false_type {};

/**
 * Specialization for const
 */
// normal fn
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const> : std::true_type {};
// variadic fn
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const> : std::true_type {};

// volatile
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile> : std::true_type {};
// ref qualified
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const&&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const&&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile&&> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile&&> : std::true_type {
};
// noexcept
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const&& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const&& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_const_function<Ret(Args...) const volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_const_function<Ret(Args..., ...) const volatile&& noexcept>
    : std::true_type {};

/**
 * Specialization for volatile
 */
// normal fn
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile> : std::true_type {};
// variadic fn
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile> : std::true_type {};

// const
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const> : std::true_type {
};
// ref qualified
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const&>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile&&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile&&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const&&> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const&&>
    : std::true_type {};
// noexcept
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile&& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args...) volatile const&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_volatile_function<Ret(Args..., ...) volatile const&& noexcept>
    : std::true_type {};

/**
 * Specialization for noexcept
 */
// normal fn
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) noexcept> : std::true_type {};
// variadic fn
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) noexcept> : std::true_type {};

// const
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const noexcept> : std::true_type {
};
// ref qualified
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) & noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) & noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) && noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) && noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const&& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const&& noexcept>
    : std::true_type {};
// volatile
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) volatile noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const volatile noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) volatile& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) volatile&& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args...) const volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_noexcept_function<Ret(Args..., ...) const volatile&& noexcept>
    : std::true_type {};

/**
 * Specialization for lvalue ref-qualifier
 */
// normal fn
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...)&> : std::true_type {};
// variadic fn
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...)&> : std::true_type {};

// const
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) const&> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) const&> : std::true_type {};
// ref qualified
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) & noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) & noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) const& noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) const& noexcept>
    : std::true_type {};
// volatile
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) const volatile&> : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) const volatile&>
    : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args...) const volatile& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_lvalue_ref_function<Ret(Args..., ...) const volatile& noexcept>
    : std::true_type {};

/**
 * Specialization for rvalue ref-qualifier
 */
// normal fn
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) &&> : std::true_type {};
// variadic fn
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) &&> : std::true_type {};

// const
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) const&&> : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) const&&> : std::true_type {};
// ref qualified
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) && noexcept> : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) && noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) const&& noexcept> : std::true_type {
};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) const&& noexcept>
    : std::true_type {};
// volatile
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) volatile&&> : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) volatile&&> : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) const volatile&&> : std::true_type {
};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) const volatile&&>
    : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args...) const volatile&& noexcept>
    : std::true_type {};
template <class Ret, class... Args>
struct is_rvalue_ref_function<Ret(Args..., ...) const volatile&& noexcept>
    : std::true_type {};
}  // namespace detail
}  // namespace fn_type_traits
#endif  // ifndef _DETECT_FN_TRAITS_HPP_
