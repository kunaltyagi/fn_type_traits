#ifndef _INFO_FN_TRAITS_HPP_
#define _INFO_FN_TRAITS_HPP_

#include <cstddef>
#include <tuple>
#include <type_traits>

#include "base_fn_traits.hpp"
#include "remove_fn_traits.hpp"

namespace fn_type_traits {
namespace detail {
template <typename>
struct return_type_fn {};
template <typename Ret, typename... Args>
struct return_type_fn<Ret(Args...)> {
    using type = Ret;
};
}  // namespace detail

template <typename Type, typename = std::enable_if_t<is_fn<Type>::value>>
struct return_type_fn
    : detail::return_type_fn<detail::make_canonical_fn_t<Type>> {};
template <typename Type>
using return_type_fn_t = typename return_type_fn<Type>::type;

namespace detail {
template <typename>
struct arity_fn {};
template <typename Ret, typename... Args>
struct arity_fn<Ret(Args...)> {
    static constexpr std::size_t value = sizeof...(Args);
};
}  // namespace detail

template <typename Type, typename = std::enable_if_t<is_fn<Type>::value>>
struct arity_fn : detail::arity_fn<detail::make_canonical_fn_t<Type>> {
    using detail::arity_fn<detail::make_canonical_fn_t<Type>>::value;
};
template <typename Type>
inline constexpr auto arity_fn_v = arity_fn<Type>::value;

namespace detail {
template <typename, std::size_t>
struct nth_parameter_fn_impl {};
template <typename Ret, typename... Args, std::size_t N>
struct nth_parameter_fn_impl<Ret(Args...), N> {
    using type = typename std::tuple_element<N, std::tuple<Args...>>::type;
};
template <typename Type,
          std::size_t N,
          typename = std::enable_if_t<is_fn<Type>::value>>
struct nth_parameter_fn
    : nth_parameter_fn_impl<detail::make_canonical_fn_t<Type>, N> {};
template <typename Type, std::size_t N>
using nth_parameter_fn_t = typename nth_parameter_fn<Type, N>::type;
}  // namespace detail

template <std::size_t N, typename Type>
struct nth_parameter_type_fn : detail::nth_parameter_fn<Type, N> {};
template <std::size_t N, typename Type>
using nth_parameter_type_fn_t = typename nth_parameter_type_fn<N, Type>::type;

namespace detail {
template <typename>
struct parameter_size_fn {};
template <typename Ret, typename... Args>
struct parameter_size_fn<Ret(Args...)> {
    static constexpr std::size_t value = (sizeof(Args) + ...);
};
}  // namespace detail
template <typename Type, typename = std::enable_if_t<is_fn<Type>::value>>
struct parameter_size_fn
    : parameter_size_fn<detail::make_canonical_fn_t<Type>> {};
template <typename Type>
inline constexpr auto parameter_size_fn_v = parameter_size_fn<Type>::value;
}  // namespace fn_type_traits
#endif  // ifndef _INFO_FN_TRAITS_HPP_
