#ifndef _REMOVE_FN_TRAITS_HPP_
#define _REMOVE_FN_TRAITS_HPP_

#include <type_traits>

#include "base_fn_traits.hpp"

namespace fn_type_traits {
namespace detail {
template <class>
struct remove_const_function;
template <class>
struct remove_volatile_function;
template <class>
struct remove_noexcept_function;
template <class>
struct remove_lvalue_ref_function;
template <class>
struct remove_rvalue_ref_function;
}  // namespace detail

template <class T>
struct remove_const_fn
    : detail::apply_trait_fn<detail::remove_const_function, T> {};
template <class T>
using remove_const_fn_t = typename remove_const_fn<T>::type;

template <class T>
struct remove_volatile_fn
    : detail::apply_trait_fn<detail::remove_volatile_function, T> {};
template <class T>
using remove_volatile_fn_t = typename remove_volatile_fn<T>::type;

template <class T>
struct remove_noexcept_fn
    : detail::apply_trait_fn<detail::remove_noexcept_function, T> {};
template <class T>
using remove_noexcept_fn_t = typename remove_noexcept_fn<T>::type;

namespace detail {
// namespace added because the standard doesn't have is_{l,r}value_reference (yet)
template <class T>
struct remove_lvalue_reference_fn
    : detail::apply_trait_fn<detail::remove_lvalue_ref_function, T> {};
template <class T>
using remove_lvalue_reference_fn_t =
    typename remove_lvalue_reference_fn<T>::type;

template <class T>
struct remove_rvalue_reference_fn
    : detail::apply_trait_fn<detail::remove_rvalue_ref_function, T> {};
template <class T>
using remove_rvalue_reference_fn_t =
    typename remove_rvalue_reference_fn<T>::type;
} // namespace detail

template <class T>
struct remove_cv_fn : remove_const_fn<remove_volatile_fn_t<T>> {};
template <class T>
using remove_cv_fn_t = typename remove_cv_fn<T>::type;

template <class T>
struct remove_reference_fn
    : detail::remove_lvalue_reference_fn<detail::remove_rvalue_reference_fn_t<T>> {};
template <class T>
using remove_reference_fn_t = typename remove_reference_fn<T>::type;

template <class T>
struct remove_cvref_fn : remove_reference_fn<remove_cv_fn_t<T>> {};
template <class T>
using remove_cvref_fn_t = typename remove_cvref_fn<T>::type;

namespace detail {
template <class T>
struct remove_all_qualifiers_fn: remove_noexcept_fn<remove_cvref_fn_t<T>> {};
template <class T>
using remove_all_qualifiers_fn_t =
    typename remove_all_qualifiers_fn<T>::type;

template <class T>
struct make_canonical_fn: remove_all_qualifiers_fn<strip_class_t<T>> {};
template <class T>
using make_canonical_fn_t =
    typename make_canonical_fn<T>::type;

template <class T>
struct remove_const_function {
    using type = T;
};

template <class T>
struct remove_volatile_function {
    using type = T;
};

template <class T>
struct remove_noexcept_function {
    using type = T;
};

template <class T>
struct remove_lvalue_ref_function {
    using type = T;
};

template <class T>
struct remove_rvalue_ref_function {
    using type = T;
};

/**
 * Specialization for const
 */
// normal fn
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const> {
    using type = Ret(Args...);
};
// variadic fn
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const> {
    using type = Ret(Args..., ...);
};

// volatile
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile> {
    using type = Ret(Args...) volatile;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile> {
    using type = Ret(Args..., ...) volatile;
};
// ref qualified
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const&> {
    using type = Ret(Args...) &;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const&> {
    using type = Ret(Args..., ...) &;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile&> {
    using type = Ret(Args...) volatile&;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile&> {
    using type = Ret(Args..., ...) volatile&;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const&&> {
    using type = Ret(Args...) &&;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const&&> {
    using type = Ret(Args..., ...) &&;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile&&> {
    using type = Ret(Args...) volatile&&;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile&&> {
    using type = Ret(Args..., ...) volatile&&;
};
// noexcept
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const noexcept> {
    using type = Ret(Args...) noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const noexcept> {
    using type = Ret(Args..., ...) noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile noexcept> {
    using type = Ret(Args...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile noexcept> {
    using type = Ret(Args..., ...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const& noexcept> {
    using type = Ret(Args...) & noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const& noexcept> {
    using type = Ret(Args..., ...) & noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile& noexcept> {
    using type = Ret(Args...) volatile& noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile& noexcept> {
    using type = Ret(Args..., ...) volatile& noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const&& noexcept> {
    using type = Ret(Args...) && noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const&& noexcept> {
    using type = Ret(Args..., ...) && noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args...) const volatile&& noexcept> {
    using type = Ret(Args...) volatile&& noexcept;
};
template <class Ret, class... Args>
struct remove_const_function<Ret(Args..., ...) const volatile&& noexcept> {
    using type = Ret(Args..., ...) volatile&& noexcept;
};

/**
 * Specialization for volatile
 */
// normal fn
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile> {
    using type = Ret(Args...);
};
// variadic fn
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile> {
    using type = Ret(Args..., ...);
};

// const
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile> {
    using type = Ret(Args...) const;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile> {
    using type = Ret(Args..., ...) const;
};
// ref qualified
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile&> {
    using type = Ret(Args...) &;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile&> {
    using type = Ret(Args..., ...) &;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile&> {
    using type = Ret(Args...) const&;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile&> {
    using type = Ret(Args..., ...) const&;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile&&> {
    using type = Ret(Args...) &&;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile&&> {
    using type = Ret(Args..., ...) &&;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile&&> {
    using type = Ret(Args...) const&&;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile&&> {
    using type = Ret(Args..., ...) const&&;
};
// noexcept
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile noexcept> {
    using type = Ret(Args...) noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile noexcept> {
    using type = Ret(Args..., ...) noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile noexcept> {
    using type = Ret(Args...) const noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile noexcept> {
    using type = Ret(Args..., ...) const noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile& noexcept> {
    using type = Ret(Args...) & noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile& noexcept> {
    using type = Ret(Args..., ...) & noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile& noexcept> {
    using type = Ret(Args...) const& noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile& noexcept> {
    using type = Ret(Args..., ...) const& noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) volatile&& noexcept> {
    using type = Ret(Args...) && noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) volatile&& noexcept> {
    using type = Ret(Args..., ...) && noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args...) const volatile&& noexcept> {
    using type = Ret(Args...) const&& noexcept;
};
template <class Ret, class... Args>
struct remove_volatile_function<Ret(Args..., ...) const volatile&& noexcept> {
    using type = Ret(Args..., ...) const&& noexcept;
};

/**
 * Specialization for noexcept
 */
// normal fn
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) noexcept> {
    using type = Ret(Args...);
};
// variadic fn
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) noexcept> {
    using type = Ret(Args..., ...);
};

// const
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const noexcept> {
    using type = Ret(Args...) const;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const noexcept> {
    using type = Ret(Args..., ...) const;
};
// ref qualified
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) & noexcept> {
    using type = Ret(Args...) &;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) & noexcept> {
    using type = Ret(Args..., ...) &;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const& noexcept> {
    using type = Ret(Args...) const&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const& noexcept> {
    using type = Ret(Args..., ...) const&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) && noexcept> {
    using type = Ret(Args...) &&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) && noexcept> {
    using type = Ret(Args..., ...) &&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const&& noexcept> {
    using type = Ret(Args...) const&&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const&& noexcept> {
    using type = Ret(Args..., ...) const&&;
};
// volatile
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) volatile noexcept> {
    using type = Ret(Args...) volatile;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) volatile noexcept> {
    using type = Ret(Args..., ...) volatile;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const volatile noexcept> {
    using type = Ret(Args...) const volatile;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const volatile noexcept> {
    using type = Ret(Args..., ...) const volatile;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) volatile& noexcept> {
    using type = Ret(Args...) volatile&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) volatile& noexcept> {
    using type = Ret(Args..., ...) volatile&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const volatile& noexcept> {
    using type = Ret(Args...) const volatile&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const volatile& noexcept> {
    using type = Ret(Args..., ...) const volatile&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) volatile&& noexcept> {
    using type = Ret(Args...) volatile&&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) volatile&& noexcept> {
    using type = Ret(Args..., ...) volatile&&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args...) const volatile&& noexcept> {
    using type = Ret(Args...) const volatile&&;
};
template <class Ret, class... Args>
struct remove_noexcept_function<Ret(Args..., ...) const volatile&& noexcept> {
    using type = Ret(Args..., ...) const volatile&&;
};

/**
 * Specialization for lvalue reference
 */
// normal fn
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...)&> {
    using type = Ret(Args...);
};
// variadic fn
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...)&> {
    using type = Ret(Args..., ...);
};

// const
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const&> {
    using type = Ret(Args...) const;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const&> {
    using type = Ret(Args..., ...) const;
};
// volatile
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) volatile&> {
    using type = Ret(Args...) volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) volatile&> {
    using type = Ret(Args..., ...) volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const volatile&> {
    using type = Ret(Args...) const volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const volatile&> {
    using type = Ret(Args..., ...) const volatile;
};
// noexcept
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) & noexcept> {
    using type = Ret(Args...) noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) & noexcept> {
    using type = Ret(Args..., ...) noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const & noexcept> {
    using type = Ret(Args...) const noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const & noexcept> {
    using type = Ret(Args..., ...) const noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) volatile & noexcept> {
    using type = Ret(Args...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) volatile & noexcept> {
    using type = Ret(Args..., ...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const volatile & noexcept> {
    using type = Ret(Args...) const volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const volatile & noexcept> {
    using type = Ret(Args..., ...) const volatile noexcept;
};

/**
 * Specialization for rvalue reference
 */
// normal fn
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) &&> {
    using type = Ret(Args...);
};
// variadic fn
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) &&> {
    using type = Ret(Args..., ...);
};
// const
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const&&> {
    using type = Ret(Args...) const;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const&&> {
    using type = Ret(Args..., ...) const;
};
// volatile
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) volatile&&> {
    using type = Ret(Args...) volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) volatile&&> {
    using type = Ret(Args..., ...) volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const volatile&&> {
    using type = Ret(Args...) const volatile;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const volatile&&> {
    using type = Ret(Args..., ...) const volatile;
};
// noexcept
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) && noexcept> {
    using type = Ret(Args...) noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) && noexcept> {
    using type = Ret(Args..., ...) noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const&& noexcept> {
    using type = Ret(Args...) const noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const&& noexcept> {
    using type = Ret(Args..., ...) const noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) volatile&& noexcept> {
    using type = Ret(Args...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) volatile&& noexcept> {
    using type = Ret(Args..., ...) volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args...) const volatile&& noexcept> {
    using type = Ret(Args...) const volatile noexcept;
};
template <class Ret, class... Args>
struct remove_lvalue_ref_function<Ret(Args..., ...) const volatile&& noexcept> {
    using type = Ret(Args..., ...) const volatile noexcept;
};
}  // namespace detail
}  // namespace fn_type_traits
#endif  // ifndef _REMOVE_FN_TRAITS_HPP_
