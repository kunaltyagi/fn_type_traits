#! /usr/bin/env sh

if [ ! -f 'CMakeLists.txt' ]; then
    echo "Missing file: 'CMakeLists.txt'"
    echo "Did you run from the root of the project?"
    exit 1
fi

mkdir -p build
cd build
rm * -fr

# gcov with clang has issues
CC=gcc CXX=g++ cmake .. && cmake --build . && cmake --build . --target test

if [ $?  -ne 0 ]; then
    echo "Tests failed"
    exit 2
fi

target_dir=`find . -name *.gcda | xargs -n1 dirname`
mkdir -p coverage_result
pwd

for dir in $target_dir
do
    echo "Working in $dir"
    # This depends on CMake conventions
    name=`basename $dir | sed 's/\.dir//'`
    lcov -t "coverage_result" -o $name.info -c -d $dir
    genhtml -o coverage_result/$name $name.info
done

echo "
Coverage data generated.

Check the results by running
cd build/coverage_result
python3 -m http.result 8081
"
