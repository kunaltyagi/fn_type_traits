cmake_minimum_required(VERSION 3.2)
project(fn_type_trait_tests)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_FLAGS "-g --coverage -O0")

add_executable(detect_test detect_test.cpp)
target_link_libraries(detect_test ${GTEST_BOTH_LIBRARIES})

add_executable(base_test base_test.cpp)
target_link_libraries(base_test ${GTEST_BOTH_LIBRARIES})

add_executable(add_test add_test.cpp)
target_link_libraries(add_test ${GTEST_BOTH_LIBRARIES})

add_executable(remove_test remove_test.cpp)
target_link_libraries(remove_test ${GTEST_BOTH_LIBRARIES})

add_executable(info_test info_test.cpp)
target_link_libraries(info_test ${GTEST_BOTH_LIBRARIES})

add_test(detection detect_test)
add_test(basic base_test)
add_test(addition add_test)
add_test(removal remove_test)
add_test(info info_test)
