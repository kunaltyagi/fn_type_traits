#ifndef _ADD_FN_TRAIT_TESTS_HPP_
#define _ADD_FN_TRAIT_TESTS_HPP_

#include "gtest/gtest.h"

#include <fn_type_traits/add_fn_traits.hpp>

using namespace fn_type_traits;

namespace impl {
template <typename T, template <class> class Trait>
struct is_same : std::is_same<T, typename Trait<T>::type> {};

template <typename T, template <class> class Trait>
inline constexpr bool is_same_v = is_same<T, Trait>::value;
}  // namespace impl

TEST(add_const_fn, non_fn) {
    bool value;
    struct dummy {
        const int bar;
    };
    value = impl::is_same_v<int const, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::bar)), add_const_fn>;
    EXPECT_TRUE(value);
}

TEST(add_volatile_fn, non_fn) {
    bool value;
    struct dummy {
        volatile int bar;
    };
    value = impl::is_same_v<int volatile, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::bar)), add_volatile_fn>;
    EXPECT_TRUE(value);
}

TEST(add_noexcept_fn, non_fn) {
    bool value;
    struct dummy {
        int bar;
    };
    value = impl::is_same_v<int, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::bar)), add_noexcept_fn>;
    EXPECT_TRUE(value);
}

TEST(add_const_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) const, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile &noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile &&noexcept, add_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int), add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &&, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) noexcept, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) & noexcept, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, add_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v < int(int, int) && noexcept, add_const_fn > ;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, add_const_fn>;
    EXPECT_FALSE(value);
}

TEST(add_volatile_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) volatile, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, add_volatile_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &noexcept,
                            add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &&noexcept,
                            add_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int), add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &&, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) noexcept, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const noexcept, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) & noexcept, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, add_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v < int(int, int) && noexcept, add_volatile_fn > ;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, add_volatile_fn>;
    EXPECT_FALSE(value);
}

TEST(add_noexcept_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) & noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v < int(int, int) && noexcept, add_noexcept_fn > ;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &noexcept,
                            add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &&noexcept,
                            add_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int), add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &&, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&, add_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, add_noexcept_fn>;
    EXPECT_FALSE(value);
}

TEST(add_const_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) volatile noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
        bool bar_01(int) const;
        bool bar_02(int) const volatile;
        bool bar_03(int) const &;
        bool bar_04(int) const volatile &;
        bool bar_05(int) const &&;
        bool bar_06(int) const volatile &&;
        bool bar_07(int) const noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) const &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) const &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::bar_01),
                           add_const_fn_t<decltype(&dummy_1::foo_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_02),
                           add_const_fn_t<decltype(&dummy_1::foo_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_03),
                           add_const_fn_t<decltype(&dummy_1::foo_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_04),
                           add_const_fn_t<decltype(&dummy_1::foo_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_05),
                           add_const_fn_t<decltype(&dummy_1::foo_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_06),
                           add_const_fn_t<decltype(&dummy_1::foo_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_07),
                           add_const_fn_t<decltype(&dummy_1::foo_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_08),
                           add_const_fn_t<decltype(&dummy_1::foo_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_09),
                           add_const_fn_t<decltype(&dummy_1::foo_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_10),
                           add_const_fn_t<decltype(&dummy_1::foo_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_11),
                           add_const_fn_t<decltype(&dummy_1::foo_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_12),
                           add_const_fn_t<decltype(&dummy_1::foo_12)>>;
    EXPECT_TRUE(value);
}

TEST(add_volatile_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) const;
        bool foo_03(int) &;
        bool foo_04(int) const &;
        bool foo_05(int) &&;
        bool foo_06(int) const &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) const noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) const &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) const &&noexcept;
        bool bar_01(int) volatile;
        bool bar_02(int) const volatile;
        bool bar_03(int) volatile &;
        bool bar_04(int) const volatile &;
        bool bar_05(int) volatile &&;
        bool bar_06(int) const volatile &&;
        bool bar_07(int) volatile noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) volatile &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) volatile &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::bar_01),
                           add_volatile_fn_t<decltype(&dummy_1::foo_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_02),
                           add_volatile_fn_t<decltype(&dummy_1::foo_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_03),
                           add_volatile_fn_t<decltype(&dummy_1::foo_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_04),
                           add_volatile_fn_t<decltype(&dummy_1::foo_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_05),
                           add_volatile_fn_t<decltype(&dummy_1::foo_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_06),
                           add_volatile_fn_t<decltype(&dummy_1::foo_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_07),
                           add_volatile_fn_t<decltype(&dummy_1::foo_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_08),
                           add_volatile_fn_t<decltype(&dummy_1::foo_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_09),
                           add_volatile_fn_t<decltype(&dummy_1::foo_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_10),
                           add_volatile_fn_t<decltype(&dummy_1::foo_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_11),
                           add_volatile_fn_t<decltype(&dummy_1::foo_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_12),
                           add_volatile_fn_t<decltype(&dummy_1::foo_12)>>;
    EXPECT_TRUE(value);
}

TEST(add_noexcept_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile && ;
        bool foo_07(int) const;
        bool foo_08(int) const volatile ;
        bool foo_09(int) const &;
        bool foo_10(int) const volatile &;
        bool foo_11(int) const &&;
        bool foo_12(int) const volatile &&;
        bool bar_01(int) noexcept;
        bool bar_02(int) volatile noexcept;
        bool bar_03(int) & noexcept;
        bool bar_04(int) volatile &noexcept;
        bool bar_05(int) && noexcept;
        bool bar_06(int) volatile &&noexcept;
        bool bar_07(int) const noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) const &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) const &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::bar_01),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_02),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_03),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_04),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_05),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_06),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_07),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_08),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_09),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_10),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_11),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::bar_12),
                           add_noexcept_fn_t<decltype(&dummy_1::foo_12)>>;
    EXPECT_TRUE(value);
}
#endif /* ifndef _ADD_FN_TRAIT_TESTS_HPP_ */
