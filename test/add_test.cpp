#include <gtest/gtest.h>

#include <fn_type_traits/add_fn_traits.hpp>

#include "add_fn_trait_tests.hpp"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
