#ifndef _BASE_FN_TRAIT_TESTS_HPP_
#define _BASE_FN_TRAIT_TESTS_HPP_

#include <type_traits>

#include "gtest/gtest.h"

#include <fn_type_traits/base_fn_traits.hpp>

using namespace fn_type_traits;

TEST(strip_class, non_class) {
    using namespace fn_type_traits::detail;
    bool value;

    value = std::is_same_v<strip_class_t<int>, int>;
    EXPECT_TRUE(value);
    value = std::is_same_v<strip_class_t<int *const>, int *const>;
    EXPECT_TRUE(value);
    value =
        std::is_same_v<strip_class_t<volatile int *const>, volatile int *const>;
    EXPECT_TRUE(value);

    value = std::is_same_v<strip_class_t<int()>, int()>;
    EXPECT_TRUE(value);
    value =
        std::is_same_v<strip_class_t<int *( bool )const>, int *( bool )const>;
    EXPECT_TRUE(value);
    value = std::is_same_v<strip_class_t<volatile int(bool *) const>,
                           volatile int(bool *) const>;
    EXPECT_TRUE(value);
}

TEST(strip_class, class_members) {
    using namespace fn_type_traits::detail;
    struct dummy {
        int a;
        int *const b;
        volatile int *const c;

        int d();
        int *e(bool) const;
        volatile int *f(bool *) const;
    };
    bool value;

    value = std::is_same_v<strip_class_t<decltype(&dummy::a)>, int>;
    EXPECT_TRUE(value);
    value = std::is_same_v<strip_class_t<decltype(&dummy::b)>, int *const>;
    EXPECT_TRUE(value);
    value =
        std::is_same_v<strip_class_t<decltype(&dummy::c)>, volatile int *const>;
    EXPECT_TRUE(value);

    value = std::is_same_v<strip_class_t<decltype(&dummy::d)>, int()>;
    EXPECT_TRUE(value);
    value =
        std::is_same_v<strip_class_t<decltype(&dummy::e)>, int *( bool )const>;
    EXPECT_TRUE(value);
    value = std::is_same_v<strip_class_t<decltype(&dummy::f)>,
                           volatile int(bool *) const>;
}

TEST(is_fn, non_fn) {
    struct dummy {
        const int bar;
    };
    EXPECT_EQ(false, is_fn_v<int const>);
    EXPECT_EQ(false, is_fn_v<decltype(&dummy::bar)>);
}

TEST(is_fn, simple_fn) {
    EXPECT_EQ(true, is_fn_v<int(int, int)>);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile>);
    EXPECT_EQ(true, is_fn_v<int(int, int) &>);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(true, is_fn_v<int(int, int) &&>);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(true, is_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(true, is_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(true, is_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const &>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const &&>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) volatile noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
        bool foo_13(int) const;
        bool foo_14(int) const volatile;
        bool foo_15(int) const &;
        bool foo_16(int) const volatile &;
        bool foo_17(int) const &&;
        bool foo_18(int) const volatile &&;
        bool foo_19(int) const noexcept;
        bool foo_20(int) const volatile noexcept;
        bool foo_21(int) const &noexcept;
        bool foo_22(int) const volatile &noexcept;
        bool foo_23(int) const &&noexcept;
        bool foo_24(int) const volatile &&noexcept;
    };
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_09)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_10)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_11)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_12)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_13)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_14)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_15)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_16)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_17)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_18)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_19)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_20)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_21)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_22)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_23)>);
    EXPECT_EQ(true, is_fn_v<decltype(&dummy_1::foo_24)>);
}

TEST(is_free_fn, non_fn) {
    struct dummy {
        const int bar;
    };
    EXPECT_EQ(false, is_free_fn_v<int const>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy::bar)>);
}

TEST(is_free_fn, simple_fn) {
    EXPECT_EQ(true, is_free_fn_v<int(int, int)>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) &>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) &&>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(true, is_free_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const &>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const &&>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_free_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_free_free_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) volatile noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
        bool foo_13(int) const;
        bool foo_14(int) const volatile;
        bool foo_15(int) const &;
        bool foo_16(int) const volatile &;
        bool foo_17(int) const &&;
        bool foo_18(int) const volatile &&;
        bool foo_19(int) const noexcept;
        bool foo_20(int) const volatile noexcept;
        bool foo_21(int) const &noexcept;
        bool foo_22(int) const volatile &noexcept;
        bool foo_23(int) const &&noexcept;
        bool foo_24(int) const volatile &&noexcept;
    };
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_09)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_10)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_11)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_12)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_13)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_14)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_15)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_16)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_17)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_18)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_19)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_20)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_21)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_22)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_23)>);
    EXPECT_EQ(false, is_free_fn_v<decltype(&dummy_1::foo_24)>);
}

#endif /* ifndef _BASE_FN_TRAIT_TESTS_HPP_ */
