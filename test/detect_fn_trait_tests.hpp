#ifndef _DETECT_FN_TRAIT_TESTS_HPP_
#define _DETECT_FN_TRAIT_TESTS_HPP_

#include "gtest/gtest.h"

#include <fn_type_traits/detect_fn_traits.hpp>

using namespace fn_type_traits;

TEST(is_const_fn, non_fn) {
    struct dummy {
        const int bar;
    };
    EXPECT_EQ(false, is_const_fn_v<int const>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy::bar)>);
}

TEST(is_volatile_fn, non_fn) {
    struct dummy {
        volatile int bar;
    };
    EXPECT_EQ(false, is_volatile_fn_v<int volatile>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy::bar)>);
}

TEST(is_noexcept_fn, non_fn) {
    struct dummy {
        volatile int bar;
    };
    EXPECT_EQ(false, is_noexcept_fn_v<int volatile>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy::bar)>);
}

TEST(is_reference_fn, non_fn) {
    struct dummy {
        int bar;
    };
    EXPECT_EQ(false, is_reference_fn_v<int &>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy::bar)>);
}

TEST(is_const_fn, simple_fn) {
    EXPECT_EQ(false, is_const_fn_v<int(int, int)>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) &>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) &&>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(false, is_const_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(false, is_const_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const &>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const &&>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_const_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_volatile_fn, simple_fn) {
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int)>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) &>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const &>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) &&>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const &&>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(false, is_volatile_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(false, is_volatile_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_volatile_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_noexcept_fn, simple_fn) {
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int)>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) &>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const &>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) &&>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const &&>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) volatile>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(false, is_noexcept_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_noexcept_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_reference_fn, simple_fn) {
    EXPECT_EQ(false, is_reference_fn_v<int(int, int)>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) const>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) volatile>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) const volatile>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) volatile noexcept>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) const volatile noexcept>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) noexcept>);
    EXPECT_EQ(false, is_reference_fn_v<int(int, int) const noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) volatile &>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const volatile &>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) volatile &&>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const volatile &&>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) &>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const &>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) &&>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const &&>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) & noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const &noexcept>);
    EXPECT_EQ(true, is_reference_fn_v < int(int, int) && noexcept >);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const &&noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) volatile &noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const volatile &noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) volatile &&noexcept>);
    EXPECT_EQ(true, is_reference_fn_v<int(int, int) const volatile &&noexcept>);
}

TEST(is_const_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) volatile noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
    };
    struct dummy_2 {
        bool foo_01(int) const;
        bool foo_02(int) const volatile;
        bool foo_03(int) const &;
        bool foo_04(int) const volatile &;
        bool foo_05(int) const &&;
        bool foo_06(int) const volatile &&;
        bool foo_07(int) const noexcept;
        bool foo_08(int) const volatile noexcept;
        bool foo_09(int) const &noexcept;
        bool foo_10(int) const volatile &noexcept;
        bool foo_11(int) const &&noexcept;
        bool foo_12(int) const volatile &&noexcept;
    };
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_09)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_10)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_11)>);
    EXPECT_EQ(false, is_const_fn_v<decltype(&dummy_1::foo_12)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_01)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_02)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_03)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_04)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_05)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_06)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_07)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_08)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_09)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_10)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_11)>);
    EXPECT_EQ(true, is_const_fn_v<decltype(&dummy_2::foo_12)>);
}

TEST(is_volatile_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) const;
        bool foo_03(int) &;
        bool foo_04(int) const &;
        bool foo_05(int) &&;
        bool foo_06(int) const &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) const noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) const &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) const &&noexcept;
    };
    struct dummy_2 {
        bool foo_01(int) volatile;
        bool foo_02(int) const volatile;
        bool foo_03(int) volatile &;
        bool foo_04(int) const volatile &;
        bool foo_05(int) volatile &&;
        bool foo_06(int) const volatile &&;
        bool foo_07(int) volatile noexcept;
        bool foo_08(int) const volatile noexcept;
        bool foo_09(int) volatile &noexcept;
        bool foo_10(int) const volatile &noexcept;
        bool foo_11(int) volatile &&noexcept;
        bool foo_12(int) const volatile &&noexcept;
    };
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_09)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_10)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_11)>);
    EXPECT_EQ(false, is_volatile_fn_v<decltype(&dummy_1::foo_12)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_01)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_02)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_03)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_04)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_05)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_06)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_07)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_08)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_09)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_10)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_11)>);
    EXPECT_EQ(true, is_volatile_fn_v<decltype(&dummy_2::foo_12)>);
}

TEST(is_noexcept_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) const;
        bool foo_08(int) const volatile;
        bool foo_09(int) const &;
        bool foo_10(int) const volatile &;
        bool foo_11(int) const &&;
        bool foo_12(int) const volatile &&;
    };
    struct dummy_2 {
        bool foo_01(int) noexcept;
        bool foo_02(int) volatile noexcept;
        bool foo_03(int) & noexcept;
        bool foo_04(int) volatile &noexcept;
        bool foo_05(int) && noexcept;
        bool foo_06(int) volatile &&noexcept;
        bool foo_07(int) const noexcept;
        bool foo_08(int) const volatile noexcept;
        bool foo_09(int) const &noexcept;
        bool foo_10(int) const volatile &noexcept;
        bool foo_11(int) const &&noexcept;
        bool foo_12(int) const volatile &&noexcept;
    };
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_09)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_10)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_11)>);
    EXPECT_EQ(false, is_noexcept_fn_v<decltype(&dummy_1::foo_12)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_01)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_02)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_03)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_04)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_05)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_06)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_07)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_08)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_09)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_10)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_11)>);
    EXPECT_EQ(true, is_noexcept_fn_v<decltype(&dummy_2::foo_12)>);
}

TEST(is_reference_fn, member_fn) {
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) noexcept;
        bool foo_04(int) volatile noexcept;
        bool foo_05(int) const;
        bool foo_06(int) const volatile;
        bool foo_07(int) const noexcept;
        bool foo_08(int) const volatile noexcept;
    };
    struct dummy_2 {
        bool foo_01(int) &;
        bool foo_02(int) volatile &;
        bool foo_03(int) &&;
        bool foo_04(int) volatile &&;
        bool foo_05(int) const &;
        bool foo_06(int) const volatile &;
        bool foo_07(int) const &&;
        bool foo_08(int) const volatile &&;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
        bool foo_13(int) const &noexcept;
        bool foo_14(int) const volatile &noexcept;
        bool foo_15(int) const &&noexcept;
        bool foo_16(int) const volatile &&noexcept;
    };
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_01)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_02)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_03)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_04)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_05)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_06)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_07)>);
    EXPECT_EQ(false, is_reference_fn_v<decltype(&dummy_1::foo_08)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_01)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_02)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_03)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_04)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_05)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_06)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_07)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_08)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_09)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_10)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_11)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_12)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_13)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_14)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_15)>);
    EXPECT_EQ(true, is_reference_fn_v<decltype(&dummy_2::foo_16)>);
}

#endif /* ifndef _DETECT_FN_TRAIT_TESTS_HPP_ */
