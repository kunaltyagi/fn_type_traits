#ifndef _INFO_FN_TRAIT_TESTS_HPP_
#define _INFO_FN_TRAIT_TESTS_HPP_

#include <type_traits>

#include "gtest/gtest.h"

#include <fn_type_traits/info_fn_traits.hpp>

using namespace fn_type_traits;

TEST(return_type, simple_fn) {
    bool value;
    value = std::is_same_v<int, return_type_fn_t<int()>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<bool, return_type_fn_t<bool(int, int)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<double *, return_type_fn_t<double *(int, int)>>;
    EXPECT_TRUE(value);
}

TEST(arity, simple_fn) {
    EXPECT_EQ(0, arity_fn_v<int() noexcept>);
    EXPECT_EQ(0, arity_fn_v<int(void) const>);
    EXPECT_EQ(1, arity_fn_v<int(int *) volatile noexcept>);
    EXPECT_EQ(2, arity_fn_v<int *( int, bool )&>);
    EXPECT_EQ(8, arity_fn_v<int(int, int, int, int, int, int, int, int) &&>);
    // clang-format off
    EXPECT_EQ(16, arity_fn_v<int(int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int)>);
    EXPECT_EQ(32, arity_fn_v<int(int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int)>);
    EXPECT_EQ(64, arity_fn_v<int(int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int,
                                 int, int, int, int, int, int, int, int)>);
    // clang-format on
}

TEST(nth_parameter_type, simple_fn) {
    bool value;
    value = std::is_same_v<int, nth_parameter_type_fn_t<0, bool(int) noexcept>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<int, nth_parameter_type_fn_t<1, bool(double, int)>>;
    EXPECT_TRUE(value);
    value =
        std::is_same_v<float,
                       nth_parameter_type_fn_t<2, bool(int, double, float)>>;
    EXPECT_TRUE(value);
}

TEST(parameter_size, simple_fn) {
    //    EXPECT_EQ(0, parameter_size_fn_v<int(void*, bool) const)>;
    //    EXPECT_EQ(0, parameter_size_fn_v<int(void))>;
    //    EXPECT_EQ(sizeof(void*), parameter_size_fn_v<int(void*)>);
}

#endif /* ifndef _INFO_FN_TRAIT_TESTS_HPP_ */
