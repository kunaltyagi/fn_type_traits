#ifndef _REMOVE_FN_TRAIT_TESTS_HPP_
#define _REMOVE_FN_TRAIT_TESTS_HPP_

#include "gtest/gtest.h"

#include <fn_type_traits/remove_fn_traits.hpp>

using namespace fn_type_traits;

namespace impl {
template <typename T, template <class> class Trait>
struct is_same : std::is_same<T, typename Trait<T>::type> {};

template <typename T, template <class> class Trait>
inline constexpr bool is_same_v = is_same<T, Trait>::value;
}  // namespace impl

TEST(remove_const_fn, non_fn) {
    bool value;
    struct dummy {
        const int foo;
    };
    value = impl::is_same_v<int const, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::foo)), remove_const_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_volatile_fn, non_fn) {
    bool value;
    struct dummy {
        volatile int foo;
    };
    value = impl::is_same_v<int volatile, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::foo)), remove_volatile_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_noexcept_fn, non_fn) {
    bool value;
    struct dummy {
        int foo;
    };
    value = impl::is_same_v<int, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::foo)), remove_noexcept_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_reference_fn, non_fn) {
    bool value;
    struct dummy {
        int foo;
    };
    value = impl::is_same_v<int&, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<decltype(&(dummy::foo)), remove_reference_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_const_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) const, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value =
        impl::is_same_v<int(int, int) const volatile &noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value =
        impl::is_same_v<int(int, int) const volatile &&noexcept, remove_const_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int), remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &&, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) noexcept, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) & noexcept, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, remove_const_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v < int(int, int) && noexcept, remove_const_fn > ;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, remove_const_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_volatile_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) volatile, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &noexcept,
                            remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&noexcept,
                            remove_volatile_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int), remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &&, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) noexcept, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const noexcept, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) & noexcept, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, remove_volatile_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v < int(int, int) && noexcept, remove_volatile_fn > ;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, remove_volatile_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_noexcept_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) & noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v < int(int, int) && noexcept, remove_noexcept_fn > ;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &noexcept,
                            remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&noexcept,
                            remove_noexcept_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int), remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) &&, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const &&, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile &&, remove_noexcept_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, remove_noexcept_fn>;
    EXPECT_TRUE(value);
}

TEST(remove_reference_fn, simple_fn) {
    bool value;
    value = impl::is_same_v<int(int, int) noexcept, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const noexcept, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile noexcept, remove_reference_fn>;
    EXPECT_TRUE(value);
    value =
        impl::is_same_v<int(int, int) const volatile noexcept, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int), remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) volatile, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) const volatile, remove_reference_fn>;
    EXPECT_TRUE(value);
    value = impl::is_same_v<int(int, int) & noexcept, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &noexcept, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v < int(int, int) && noexcept, remove_reference_fn > ;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&noexcept, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &noexcept, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &noexcept,
                            remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&noexcept, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&noexcept,
                            remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) &&, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const &&, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) volatile &&, remove_reference_fn>;
    EXPECT_FALSE(value);
    value = impl::is_same_v<int(int, int) const volatile &&, remove_reference_fn>;
    EXPECT_FALSE(value);
}

TEST(remove_const_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) volatile noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) volatile &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) volatile &&noexcept;
        bool bar_01(int) const;
        bool bar_02(int) const volatile;
        bool bar_03(int) const &;
        bool bar_04(int) const volatile &;
        bool bar_05(int) const &&;
        bool bar_06(int) const volatile &&;
        bool bar_07(int) const noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) const &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) const &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::foo_01),
                           remove_const_fn_t<decltype(&dummy_1::bar_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_02),
                           remove_const_fn_t<decltype(&dummy_1::bar_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_03),
                           remove_const_fn_t<decltype(&dummy_1::bar_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_04),
                           remove_const_fn_t<decltype(&dummy_1::bar_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_05),
                           remove_const_fn_t<decltype(&dummy_1::bar_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_06),
                           remove_const_fn_t<decltype(&dummy_1::bar_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_07),
                           remove_const_fn_t<decltype(&dummy_1::bar_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_08),
                           remove_const_fn_t<decltype(&dummy_1::bar_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_09),
                           remove_const_fn_t<decltype(&dummy_1::bar_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_10),
                           remove_const_fn_t<decltype(&dummy_1::bar_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_11),
                           remove_const_fn_t<decltype(&dummy_1::bar_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_12),
                           remove_const_fn_t<decltype(&dummy_1::bar_12)>>;
    EXPECT_TRUE(value);
}

TEST(remove_volatile_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) const;
        bool foo_03(int) &;
        bool foo_04(int) const &;
        bool foo_05(int) &&;
        bool foo_06(int) const &&;
        bool foo_07(int) noexcept;
        bool foo_08(int) const noexcept;
        bool foo_09(int) & noexcept;
        bool foo_10(int) const &noexcept;
        bool foo_11(int) && noexcept;
        bool foo_12(int) const &&noexcept;
        bool bar_01(int) volatile;
        bool bar_02(int) const volatile;
        bool bar_03(int) volatile &;
        bool bar_04(int) const volatile &;
        bool bar_05(int) volatile &&;
        bool bar_06(int) const volatile &&;
        bool bar_07(int) volatile noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) volatile &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) volatile &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::foo_01),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_02),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_03),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_04),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_05),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_06),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_07),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_08),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_09),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_10),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_11),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_12),
                           remove_volatile_fn_t<decltype(&dummy_1::bar_12)>>;
    EXPECT_TRUE(value);
}

TEST(remove_noexcept_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) volatile;
        bool foo_03(int) &;
        bool foo_04(int) volatile &;
        bool foo_05(int) &&;
        bool foo_06(int) volatile && ;
        bool foo_07(int) const;
        bool foo_08(int) const volatile ;
        bool foo_09(int) const &;
        bool foo_10(int) const volatile &;
        bool foo_11(int) const &&;
        bool foo_12(int) const volatile &&;
        bool bar_01(int) noexcept;
        bool bar_02(int) volatile noexcept;
        bool bar_03(int) & noexcept;
        bool bar_04(int) volatile &noexcept;
        bool bar_05(int) && noexcept;
        bool bar_06(int) volatile &&noexcept;
        bool bar_07(int) const noexcept;
        bool bar_08(int) const volatile noexcept;
        bool bar_09(int) const &noexcept;
        bool bar_10(int) const volatile &noexcept;
        bool bar_11(int) const &&noexcept;
        bool bar_12(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::foo_01),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_02),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_03),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_04),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_05),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_06),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_07),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_08),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_09),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_10),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_11),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_12),
                           remove_noexcept_fn_t<decltype(&dummy_1::bar_12)>>;
    EXPECT_TRUE(value);
}

TEST(remove_reference_fn, member_fn) {
    bool value;
    struct dummy_1 {
        bool foo_01(int);
        bool foo_02(int) const;
        bool foo_03(int) volatile;
        bool foo_04(int) const volatile ;
        bool foo_05(int) noexcept;
        bool foo_06(int) volatile noexcept;
        bool foo_07(int) const noexcept;
        bool foo_08(int) const volatile noexcept;
        bool bar_01(int) &;
        bool bar_02(int) &&;
        bool bar_03(int) const &;
        bool bar_04(int) const &&;
        bool bar_05(int) volatile &;
        bool bar_06(int) volatile && ;
        bool bar_07(int) const volatile &;
        bool bar_08(int) const volatile &&;
        bool bar_09(int) & noexcept;
        bool bar_10(int) && noexcept;
        bool bar_11(int) volatile &noexcept;
        bool bar_12(int) volatile &&noexcept;
        bool bar_13(int) const &noexcept;
        bool bar_14(int) const &&noexcept;
        bool bar_15(int) const volatile &noexcept;
        bool bar_16(int) const volatile &&noexcept;
    };
    value = std::is_same_v<decltype(&dummy_1::foo_01),
                           remove_reference_fn_t<decltype(&dummy_1::bar_01)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_01),
                           remove_reference_fn_t<decltype(&dummy_1::bar_02)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_02),
                           remove_reference_fn_t<decltype(&dummy_1::bar_03)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_02),
                           remove_reference_fn_t<decltype(&dummy_1::bar_04)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_03),
                           remove_reference_fn_t<decltype(&dummy_1::bar_05)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_03),
                           remove_reference_fn_t<decltype(&dummy_1::bar_06)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_04),
                           remove_reference_fn_t<decltype(&dummy_1::bar_07)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_04),
                           remove_reference_fn_t<decltype(&dummy_1::bar_08)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_05),
                           remove_reference_fn_t<decltype(&dummy_1::bar_09)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_05),
                           remove_reference_fn_t<decltype(&dummy_1::bar_10)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_06),
                           remove_reference_fn_t<decltype(&dummy_1::bar_11)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_06),
                           remove_reference_fn_t<decltype(&dummy_1::bar_12)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_07),
                           remove_reference_fn_t<decltype(&dummy_1::bar_13)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_07),
                           remove_reference_fn_t<decltype(&dummy_1::bar_14)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_08),
                           remove_reference_fn_t<decltype(&dummy_1::bar_15)>>;
    EXPECT_TRUE(value);
    value = std::is_same_v<decltype(&dummy_1::foo_08),
                           remove_reference_fn_t<decltype(&dummy_1::bar_16)>>;
    EXPECT_TRUE(value);
}
#endif /* ifndef _REMOVE_FN_TRAIT_TESTS_HPP_ */
