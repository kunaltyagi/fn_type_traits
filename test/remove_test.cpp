#include <gtest/gtest.h>

#include <fn_type_traits/remove_fn_traits.hpp>

#include "remove_fn_trait_tests.hpp"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
